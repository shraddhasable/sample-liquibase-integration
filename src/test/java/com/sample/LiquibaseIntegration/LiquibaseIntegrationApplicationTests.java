package com.sample.LiquibaseIntegration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.sample.LiquibaseIntegration.Entity.User;
import com.sample.LiquibaseIntegration.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LiquibaseIntegrationApplicationTests {

	@Autowired
	private UserService userService;

	@Test
	public void contextLoads() {
	}

	@Test
	public void testGetUser() {
		User user = userService.getUserByUsername("j.torres");
		assertNotNull(user);
		assertEquals("123456", user.getPassword());
	}

}
