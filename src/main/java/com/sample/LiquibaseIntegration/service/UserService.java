package com.sample.LiquibaseIntegration.service;

import com.sample.LiquibaseIntegration.Entity.User;

public interface UserService {

	User getUserByUsername(String username);

}
